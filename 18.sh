#!/bin/sh

###############################################################################
# Αρχείο: 18.sh
# Υπολογισμός ημερών μέχρι το τέλος του έτους (YEAR)
#
# Φοιτητής: Αριστοτέλης Χρήστου
# Μητρώο: ΜΠΠΛ17061
# E-mail: telischristou@gmail.com
# Git: https://gitlab.com/aristotelisch/ergasthrio-leitourgika-sys-ergasia-2.git
################################################################################

YEAR=$1

# Ελέγχουμε αν έχει δοθεί παράμετρος κατά την εκτέλεση.
if [ -z "$YEAR" ]; then
	# Αν δεν έχει δοθεί θέτουμε ως YEAR το τρέχον έτος.
	YEAR=$(date -u +%Y)
fi

# Υπολογισμός ημερών μέχρι το τέλος του έτους(YEAR). (Τρέχον έτος ως προεπιλογή)
days_to_end() {
	# Δηλώση σταθερών που θα χρησιμοποιήσουμε στον υπολογισμό ημερών.
	readonly SECS_OF_DAY=3600 # Αριθμός δευτερολέπτων σε μια ώρα.
	readonly HOURS_OF_DAY=24 # Αριθμός ωρών σε μια ημέρα

	# Δηλωση τοπικών μεταβλητών με τον αριθμό δευτερολέπτων από το Unix epoc
	local SecondsToNow=$(date -u +%s) # Αριθμός δευτερολέπτων μέχρι τώρα.
	# Αριθμός δευτερολέπτων μέχρι το τέλος του δηλωθέντος έτους. (Default το τρέχον
	# έτος).
	local SecondsToEnd=$(date -u +%s -d "$YEAR-12-31 23:59:59")

	printf "Days until end of $YEAR: "
	echo $(( ($SecondsToEnd - $SecondsToNow)/(SECS_OF_DAY * HOURS_OF_DAY) ))
} 

# Κληση της συνάρτησης υπολογισμού των ημερών.
days_to_end


